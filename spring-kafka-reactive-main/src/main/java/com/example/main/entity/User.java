package com.example.main.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("m_user")
@Getter
@Setter
public class User {
	
    @Id
    private Long id;
    
    private String name;
    
    private String email;
    
}