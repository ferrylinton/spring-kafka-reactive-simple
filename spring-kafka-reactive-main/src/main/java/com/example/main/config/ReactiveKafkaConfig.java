package com.example.main.config;

import java.util.Collections;
import java.util.Map;

import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;

import com.example.common.configuration.AppProperties;
import com.example.common.dto.UserDto;
import com.example.common.model.Event;

import lombok.RequiredArgsConstructor;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.SenderOptions;

@RequiredArgsConstructor
//@Configuration
public class ReactiveKafkaConfig {

	private final AppProperties appProperties;
	
	@Bean
	public ReceiverOptions<String, Event<Long, UserDto>> kafkaReceiverOptions(KafkaProperties kafkaProperties) {
		ReceiverOptions<String, Event<Long, UserDto>> basicReceiverOptions = ReceiverOptions
				.create(kafkaProperties.buildConsumerProperties());
		return basicReceiverOptions.subscription(Collections.singletonList(appProperties.getMainTopic()));
	}

	@Bean
	public ReactiveKafkaConsumerTemplate<String, Event<Long, UserDto>> reactiveKafkaConsumerTemplate(
			ReceiverOptions<String, Event<Long, UserDto>> kafkaReceiverOptions) {
		return new ReactiveKafkaConsumerTemplate<String, Event<Long, UserDto>>(kafkaReceiverOptions);
	}
	
	@Bean
    public ReactiveKafkaProducerTemplate<String, Event<Long, UserDto>> reactiveKafkaProducerTemplate(
            KafkaProperties properties) {
        Map<String, Object> props = properties.buildProducerProperties();
        return new ReactiveKafkaProducerTemplate<String, Event<Long, UserDto>>(SenderOptions.create(props));
    }
}
