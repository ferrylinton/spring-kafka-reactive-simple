package com.example.main.controller.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.main.entity.User;
import com.example.main.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class UserRestController {

	private final UserRepository userRepository;

	@GetMapping("/users/{id}")
	Mono<User> getUser(@PathVariable("id") Long id) {
		return userRepository.findById(id);
	}
	
}
