package com.example.main.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;

import com.example.main.entity.User;

public interface UserRepository extends R2dbcRepository<User, Long>{

}
